import java.util.Scanner;

public class e8893 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        if (n >= 10 && n <= 99 && n % 2 == 0 && n % 3 == 0)
            System.out.println("YES");
        else
            System.out.println("NO");
    }
}
//80% eolymp
