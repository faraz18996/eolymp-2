import java.util.Scanner;

public class e8894 {
    public static void main(String[] args) {
        Scanner in= new Scanner(System.in);
        int n= in.nextInt();
        if ((n>=0 && n%2==0)||(n<0 && n%2!=0))
            System.out.println("NO");
        else
            System.out.println("YES");
    }
}
